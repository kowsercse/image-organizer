package com.kowsercse.organizer.domain;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.StringJoiner;

public class ImageInfo {

  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");
  private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH-mm-ss");
  private static final DateTimeFormatter MONTH_FORMATTER = DateTimeFormatter.ofPattern("MM");

  private TiffField model;
  private TiffField maker;
  private TiffField captureTime;
  private TiffField originalDateTime;

  private File file;
  private boolean sameTimeDuplicate = false;
  private boolean moved;

  public boolean isMoved() {
    return moved;
  }

  public void setMoved(final boolean moved) {
    this.moved = moved;
  }

  public TiffField getCaptureTime() {
    return captureTime;
  }

  public void setCaptureTime(final TiffField captureTime) {
    this.captureTime = captureTime;
  }

  public TiffField getModel() {
    return model;
  }

  public void setModel(final TiffField model) {
    this.model = model;
  }

  public TiffField getMaker() {
    return maker;
  }

  public void setMaker(final TiffField maker) {
    this.maker = maker;
  }

  public TiffField getOriginalDateTime() {
    return originalDateTime;
  }

  public void setOriginalDateTime(final TiffField originalDateTime) {
    this.originalDateTime = originalDateTime;
  }

  public long getSize() {
    return file.length();
  }

  public void markSameTimeDuplicate() {
    this.sameTimeDuplicate = true;
  }

  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }

  @Override
  public String toString() {
    return file.getAbsolutePath();
  }

  public String getExtension() {
    return FilenameUtils.getExtension(getFile().getAbsolutePath());
  }

  public String getModelDescription() {
    return Optional.ofNullable(model)
        .map(TiffField::getValueDescription)
        .orElse(null);
  }

  public LocalDateTime getCaptureLocalTime() {
    String time = captureTime != null
        ? captureTime.getValueDescription()
        : originalDateTime != null ? originalDateTime.getValueDescription() : null;
    return time != null ? parseTime(time) : null;
  }

  public String getRelativePath() {
    final LocalDateTime time = getCaptureLocalTime();
    final String datePart = time.format(DATE_FORMATTER);
    final String timePart = time.format(TIME_FORMATTER);
    return new StringJoiner(File.separator, File.separator, (sameTimeDuplicate ? "_1" : "") + "." + getExtension().toUpperCase())
        .add(String.valueOf(time.getYear()))
        .add(time.format(MONTH_FORMATTER))
        .add(datePart)
        .add(getModelName())
        .add(datePart + "_" + timePart)
        .toString();
  }

  private String getModelName() {
    try {
      return getModel().getStringValue()
          .replace(",", "_")
          .replace("/", "_")
          .replace("<", "")
          .replace(">", "")
          .trim();
    }
    catch (ImageReadException e) {
      return "Unknown";
    }
  }

  private LocalDateTime parseTime(String time) {
    try {
      return LocalDateTime.parse(time.replace("'", ""), DATE_TIME_FORMATTER);
    }
    catch (RuntimeException ex) {
      return null;
    }
  }

}
