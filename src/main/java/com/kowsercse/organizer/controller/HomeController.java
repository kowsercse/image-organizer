package com.kowsercse.organizer.controller;

import com.kowsercse.organizer.service.ImageOrganizerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

  @Autowired
  private ImageOrganizerService imageOrganizerService;

  @RequestMapping("/")
  @ResponseBody
  public String index() {
    System.out.println(imageOrganizerService.toString());
    return "Greetings from Spring Boot!";
  }

}
