package com.kowsercse.organizer.service;

import com.kowsercse.organizer.configuration.MigrationConfiguration;
import com.kowsercse.organizer.configuration.ReportConfiguration;
import com.kowsercse.organizer.domain.ImageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.*;

@Component
public class ImageOrganizerService {
  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final MigrationConfiguration configuration;
  private final ReportConfiguration reportConfiguration;
  private final ImageInfoExtractor imageInfoExtractor;

  @Autowired
  public ImageOrganizerService(final MigrationConfiguration configuration, final ReportConfiguration reportConfiguration, final ImageInfoExtractor imageInfoExtractor) {
    this.configuration = configuration;
    this.reportConfiguration = reportConfiguration;
    this.imageInfoExtractor = imageInfoExtractor;
  }

  public void organize() throws IOException {
    ImageFileVisitor visitor = new ImageFileVisitor(
        imageInfoExtractor,
        configuration.isSimulation()
    );

    Files.walkFileTree(configuration.getSource().toPath(), visitor);
    final List<ImageInfo> imageInfoList = visitor.getImageInfoList();
    final List<File> failedFiles = visitor.getFailedFiles();

    System.out.println("==============================");
    System.out.println("Generating report");
    System.out.println("==============================");

    final HashMap<String, List<ImageInfo>> duplicates = new HashMap<>();
    imageInfoList.forEach(imageInfo -> duplicates.compute(
        getSafeRelativePath(imageInfo, failedFiles), (path, imageList) -> getImageInfoList(imageInfo, imageList))
    );

    moveFiles(duplicates, failedFiles);
    printSuccess(duplicates);
    printDuplicates(duplicates);
    printFailed(failedFiles);
    printSummary(imageInfoList, duplicates, failedFiles);
  }

  private void moveFiles(final HashMap<String, List<ImageInfo>> duplicates, final List<File> failedFiles) {
    duplicates.forEach((relativePath, imageInfoList) -> {
      if (imageInfoList.size() == 1) {
        if (!moveFile(imageInfoList.get(0))) {
          failedFiles.add(imageInfoList.get(0).getFile());
        }
      }
      else if (isTakenAtSameTime(imageInfoList)) {
        if (!moveFile(imageInfoList.get(0))) {
          failedFiles.add(imageInfoList.get(0).getFile());
        }

        imageInfoList.get(1).markSameTimeDuplicate();
        if (!moveFile(imageInfoList.get(1))) {
          failedFiles.add(imageInfoList.get(1).getFile());
        }
      }
      else if(isDuplicate(imageInfoList)) {
        if (!moveFile(imageInfoList.get(0))) {
          failedFiles.add(imageInfoList.get(0).getFile());
        }

        if (imageInfoList.get(1).getFile().delete()) {
          failedFiles.add(imageInfoList.get(1).getFile());
        }
      }
    });
  }

  private boolean isDuplicate(final List<ImageInfo> imageInfoList) {
    return imageInfoList.size() == 2
        && imageInfoList.get(0).getSize() == imageInfoList.get(1).getSize()
        && imageInfoList.get(0).getFile().getName().equals(imageInfoList.get(1).getFile().getName());
  }

  private boolean moveFile(final ImageInfo imageInfo) {
    imageInfo.setMoved(true);
    if (configuration.isSimulation()) {
      return true;
    }

    final File newLocation = new File(configuration.getDestination().getAbsolutePath(), imageInfo.getRelativePath());
    final File parentFile = newLocation.getParentFile();
    return (parentFile.exists() || parentFile.mkdirs()) && imageInfo.getFile().renameTo(newLocation);
  }

  private boolean isTakenAtSameTime(final List<ImageInfo> imageInfoList) {
    return imageInfoList.size() == 2
        && imageInfoList.get(0).getSize() != imageInfoList.get(1).getSize()
        && imageInfoList.get(0).getFile().getParentFile().equals(imageInfoList.get(1).getFile().getParentFile());
  }

  private List<ImageInfo> getImageInfoList(final ImageInfo imageInfo, List<ImageInfo> imageList) {
    if (imageList == null) {
      imageList = new LinkedList<>();
    }

    imageList.add(imageInfo);
    return imageList;
  }

  private String getSafeRelativePath(final ImageInfo imageInfo, final List<File> failed) {
    try {
      return imageInfo.getRelativePath();
    }
    catch (RuntimeException e) {
      logger.warn("Failed to get relative path for: " + imageInfo.getFile().getAbsolutePath(), e);
      failed.add(imageInfo.getFile());
    }
    return "";
  }

  private void printSuccess(final HashMap<String, List<ImageInfo>> duplicates) throws IOException {
    try (final FileWriter out = new FileWriter(reportConfiguration.getSuccess()); final PrintWriter output = new PrintWriter(out)) {
      duplicates.entrySet().stream()
          .map(Map.Entry::getKey)
          .sorted()
          .forEach((relativePath) -> {
            final List<ImageInfo> infoList = duplicates.get(relativePath);
            String row = new StringJoiner(",")
                .add(relativePath)
                .add(String.valueOf(infoList.get(0).isMoved()))
                .add(String.valueOf(infoList.size()))
                .add(String.valueOf(duplicates.size() > 1))
                .add(getDirectories(infoList))
                .toString();
            output.println(row);
          });
    }
  }

  private void printDuplicates(final HashMap<String, List<ImageInfo>> duplicates) throws IOException {
    try (final FileWriter out = new FileWriter(reportConfiguration.getDuplicate()); final PrintWriter output = new PrintWriter(out)) {
      duplicates.entrySet().stream()
          .map(Map.Entry::getKey)
          .sorted()
          .forEach((relativePath) -> {
            final List<ImageInfo> infoList = duplicates.get(relativePath);
            if (infoList.size() > 1) output.println(relativePath + "," + getDirectories(infoList));
          });
    }
  }

  private void printFailed(final List<File> failed) throws IOException {
    try (FileWriter out = new FileWriter(reportConfiguration.getFailure()); PrintWriter output = new PrintWriter(out)) {
      failed.stream().map(File::getAbsolutePath).sorted().forEach(output::println);
      failed.stream().map(File::getParentFile).sorted().forEach(output::println);
    }
  }

  private String getDirectories(List<ImageInfo> imageInfoList) {
    StringJoiner directoryJoiner = new StringJoiner("|");
    imageInfoList.forEach(imageInfo -> directoryJoiner.add(imageInfo.getFile().getAbsolutePath()));
    return directoryJoiner.toString();
  }

  private void printSummary(final List<ImageInfo> imageInfoList, final HashMap<String, List<ImageInfo>> duplicates, final List<File> failed) {
    double gb = imageInfoList.stream().mapToLong(ImageInfo::getSize).sum() / (1024 * 1024 * 1024);
    System.out.println(" Total image count: " + imageInfoList.size());
    System.out.println("Unique image count: " + duplicates.size());
    System.out.println("  Total image size: " + gb + " GB");
    System.out.println("Total image failed: " + failed.size());
  }

}
