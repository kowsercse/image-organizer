package com.kowsercse.organizer.service;

import com.kowsercse.organizer.domain.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class ImageInfoExtractor {

  public ImageInfo extract(final File file) throws ImageReadException, IOException {
    final ImageMetadata metadata = Imaging.getMetadata(file);
    final ImageInfo imageInfo = new ImageInfo();
    imageInfo.setFile(file);

    if (metadata instanceof TiffImageMetadata) {
      TiffImageMetadata tiffImageMetadata = (TiffImageMetadata) metadata;

      final TiffField maker = tiffImageMetadata.findField(TiffTagConstants.TIFF_TAG_MAKE);
      imageInfo.setMaker(maker);

      final TiffField model = tiffImageMetadata.findField(TiffTagConstants.TIFF_TAG_MODEL);
      imageInfo.setModel(model);

      final TiffField dateTime = tiffImageMetadata.findField(TiffTagConstants.TIFF_TAG_DATE_TIME);
      imageInfo.setCaptureTime(dateTime);

      final TiffField originalDateTime = tiffImageMetadata.findField(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
      imageInfo.setOriginalDateTime(originalDateTime);
    } else if (metadata instanceof JpegImageMetadata) {
      final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

      final TiffField maker = jpegMetadata.findEXIFValueWithExactMatch(TiffTagConstants.TIFF_TAG_MAKE);
      imageInfo.setMaker(maker);

      final TiffField model = jpegMetadata.findEXIFValueWithExactMatch(TiffTagConstants.TIFF_TAG_MODEL);
      imageInfo.setModel(model);

      final TiffField dateTime = jpegMetadata.findEXIFValueWithExactMatch(TiffTagConstants.TIFF_TAG_DATE_TIME);
      imageInfo.setCaptureTime(dateTime);

      final TiffField originalDateTime = jpegMetadata.findEXIFValueWithExactMatch(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
      imageInfo.setOriginalDateTime(originalDateTime);
    }

    return imageInfo;
  }

}
