package com.kowsercse.organizer.service;

import com.kowsercse.organizer.domain.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ImageFileVisitor extends SimpleFileVisitor<Path> {

  private static final List<String> PREFIXES = Arrays.asList(".__DSC", "._DSC", "._IMG", ".DS_Store", ".picasa", "._.DS_Store");
  private static final List<String> EXTENSIONS = Arrays.asList(".MOV", ".VPL", ".MPL", ".MTS", ".mp4", ".CPI");
  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final ImageInfoExtractor imageInfoExtractor;
  private final boolean simulating;

  private final Set<Path> ignoredPaths = new HashSet<>();
  private final List<ImageInfo> imageInfoList = new LinkedList<>();
  private final List<File> failedFiles = new LinkedList<>();

  public List<ImageInfo> getImageInfoList() {
    return imageInfoList;
  }

  public List<File> getFailedFiles() {
    return failedFiles;
  }

  public ImageFileVisitor(final ImageInfoExtractor imageInfoExtractor, final boolean simulating) {
    this.imageInfoExtractor = imageInfoExtractor;
    this.simulating = simulating;
  }

  public void addIgnoreDirectories(final Path ignorePath) {
    ignoredPaths.add(ignorePath);
  }

  @Override
  public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
    return ignoredPaths.contains(dir) ? FileVisitResult.SKIP_SUBTREE : FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult postVisitDirectory(Path visitedPath, IOException exc) throws IOException {
    File file = visitedPath.toFile();
    if (!simulating && isEmptyFolder(file)) {
      file.delete();
    }
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFile(Path path, BasicFileAttributes attr) {
    final File file = path.toFile();
    if (isHiddenFile(file)) {
      if (!simulating) {
        file.delete();
      }
    }
    else if (isVideoFile(file)) {
      failedFiles.add(file);
    }
    else if (attr.isRegularFile()) {
      try {
        ImageInfo imageInfo = imageInfoExtractor.extract(file);
        imageInfoList.add(imageInfo);
        logAndPause(imageInfoList.size());
      }
      catch (ImageReadException | IOException ex) {
        failedFiles.add(file);
        logger.warn("Failed to process: " + file.getAbsolutePath(), ex);
      }
    }

    return FileVisitResult.CONTINUE;
  }

  private boolean isVideoFile(final File file) {
    final String absolutePath = file.getAbsolutePath();
    return EXTENSIONS.stream().anyMatch(absolutePath::endsWith);
  }

  @Override
  public FileVisitResult visitFileFailed(Path path, IOException exc) {
    failedFiles.add(path.toFile());
    return FileVisitResult.CONTINUE;
  }

  private boolean isEmptyFolder(File file) {
    return (file.isDirectory() && file.list().length == 0);
  }

  private boolean isHiddenFile(File file) {
    String name = file.getName();
    return file.isFile() && (isThumbnail(name) || PREFIXES.stream().anyMatch(name::startsWith)) || name.endsWith("NEF_embedded.jpg");
  }

  private boolean isThumbnail(String name) {
    return name.startsWith(".") && name.endsWith(".jpg");
  }

  private void logAndPause(final int processed) {
    if (processed % 200 == 0) {
      System.out.println("Processed: " + processed);
    }

    if (processed % 1000 == 0) {
      try {
        Thread.sleep(TimeUnit.SECONDS.toMillis(1));
      }
      catch (InterruptedException e) {
        logger.debug("Failed to interrupt after processing: {} items.", processed);
      }
    }
  }

}
