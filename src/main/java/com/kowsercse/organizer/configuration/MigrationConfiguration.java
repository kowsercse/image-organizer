package com.kowsercse.organizer.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;

@Component
@ConfigurationProperties(prefix = "com.kowsercse.organizer.migration")
public class MigrationConfiguration {

  private boolean simulation;
  private String source;
  private String destination;
  private File sourceDir;
  private File destinationDir;

  public boolean isSimulation() {
    return simulation;
  }

  public void setSimulation(final boolean simulation) {
    this.simulation = simulation;
  }

  public File getSource() {
    return sourceDir;
  }

  public void setSource(final String source) {
    this.source = source;
  }

  public File getDestination() {
    return destinationDir;
  }

  public void setDestination(final String destination) {
    this.destination = destination;
  }

  @PostConstruct
  public void init() {
    sourceDir = new File(source);
    if (!sourceDir.exists()) {
      throw new IllegalStateException("Source directory is not available.");
    }
    destinationDir = new File(destination);
    if (!destinationDir.exists()) {
      if (!destinationDir.mkdir()) {
        throw new IllegalStateException("Failed to create destination directory");
      }
    }
  }

}
