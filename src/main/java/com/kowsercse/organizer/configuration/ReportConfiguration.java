package com.kowsercse.organizer.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

@Component
@ConfigurationProperties(prefix = "com.kowsercse.organizer.report")
public class ReportConfiguration {

  private String success;
  private String failure;
  private String duplicate;
  private File successFile;
  private File failureFile;
  private File duplicateFile;


  public File getSuccess() {
    return successFile;
  }

  public void setSuccess(final String success) {
    this.success = success;
  }

  public File getFailure() {
    return failureFile;
  }

  public void setFailure(final String failure) {
    this.failure = failure;
  }

  public File getDuplicate() {
    return duplicateFile;
  }

  public void setDuplicate(final String duplicate) {
    this.duplicate = duplicate;
  }

  @PostConstruct
  public void init() throws IOException {
    successFile = createReportFile(success);
    failureFile = createReportFile(failure);
    duplicateFile = createReportFile(duplicate);
  }

  private File createReportFile(String pathname) throws IOException {
    File csvFile = new File(pathname);
    if (csvFile.exists()) {
      csvFile.delete();
    }
    if (!csvFile.exists()) {
      csvFile.createNewFile();
    }
    return csvFile;
  }

}
