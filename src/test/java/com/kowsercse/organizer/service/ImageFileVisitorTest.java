package com.kowsercse.organizer.service;

import com.kowsercse.organizer.domain.ImageInfo;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ImageFileVisitorTest {

  private ImageFileVisitor imageFileVisitor;

  @Before
  public void setUp() {
    imageFileVisitor = new ImageFileVisitor(new ImageInfoExtractor(), true);
  }

  @Test
  public void getImageInfoList() throws URISyntaxException, IOException {
    final File images = new File(getClass().getResource("/images").toURI());
    Files.walkFileTree(images.toPath(), imageFileVisitor);
    final List<ImageInfo> imageInfoList = imageFileVisitor.getImageInfoList();
    final List<File> failedFiles = imageFileVisitor.getFailedFiles();

    assertEquals(2, imageInfoList.size());
    assertEquals(1, failedFiles.size());
  }

}
