package com.kowsercse.organizer.service;

import com.kowsercse.organizer.domain.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class ImageInfoExtractorTest {

  private ImageInfoExtractor imageInfoExtractor = new ImageInfoExtractor();

  @Test
  public void testReadCanonImage() throws URISyntaxException, IOException, ImageReadException {
    final File imageFile = new File(getClass().getResource("/images/2004-11-29_12-19-20.jpg").toURI());
    final ImageInfo imageInfo = imageInfoExtractor.extract(imageFile);
    final LocalDateTime time = imageInfo.getCaptureLocalTime();
    assertEquals(2004, time.getYear());
    assertEquals(11, time.getMonthValue());
    assertEquals(29, time.getDayOfMonth());
    assertEquals(12, time.getHour());
    assertEquals(19, time.getMinute());
    assertEquals(20, time.getSecond());
    assertEquals(20, time.getSecond());
    assertEquals("JPG", imageInfo.getExtension().toUpperCase());
    assertEquals("Canon", imageInfo.getMaker().getStringValue());
    assertEquals("Canon PowerShot S30", imageInfo.getModel().getStringValue());
  }

  @Test
  public void testImagePath() throws URISyntaxException, IOException, ImageReadException {
    final File imageFile = new File(getClass().getResource("/images/2004-11-29_12-19-20.jpg").toURI());
    final ImageInfo imageInfo = imageInfoExtractor.extract(imageFile);
    assertEquals("/2004/11/2004-11-29/Canon PowerShot S30/2004-11-29_12-19-20.JPG", imageInfo.getRelativePath());

    imageInfo.markSameTimeDuplicate();
    assertEquals("/2004/11/2004-11-29/Canon PowerShot S30/2004-11-29_12-19-20_1.JPG", imageInfo.getRelativePath());

    final File anotherFile = new File(getClass().getResource("/images/2011_01_04_08_27_49.jpg").toURI());
    final ImageInfo anotherInfo = imageInfoExtractor.extract(anotherFile);
    assertEquals("/2011/01/2011-01-04/Unknown/2011-01-04_08-27-49.JPG", anotherInfo.getRelativePath());
  }

}
