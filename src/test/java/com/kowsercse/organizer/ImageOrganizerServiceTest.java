package com.kowsercse.organizer;

import com.kowsercse.organizer.service.ImageFileVisitor;
import com.kowsercse.organizer.service.ImageOrganizerService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImageOrganizerServiceTest {

  @Autowired
  private ImageOrganizerService imageOrganizerService;

  @Test
  public void organize() throws URISyntaxException, IOException {
    imageOrganizerService.organize();
  }

}
